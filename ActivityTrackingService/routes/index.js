var express = require('express'),
    controllers = require('../controllers'),
    router = express.Router();

router.get('/', function (req, res, next) {
    res.send({
        serviceName: 'Activity Tracker Service',
        time: new Date(),
        apiVersion: "v1"
    });
});

router.get('/api/v1/categories', controllers.activity.getCategories);
router.get('/api/v1/user/:user/summary/year/current', controllers.activity.getSummaryForCurrentYear);
router.get('/api/v1/user/:user/summary/year/:year', controllers.activity.getSummaryByYear);
router.get('/api/v1/user/:user/summary/year/:year/month/:month', controllers.activity.getSummaryForMonth);
router.get('/api/v1/user/:user/summary/year/:year/month/:month/date/:date', controllers.activity.getSummaryForDate);
router.post('/api/v1/activity', controllers.activity.create);
router.get('/api/v1/seed/userId/:userId/year/:year', controllers.activity.seed);
module.exports = router;
