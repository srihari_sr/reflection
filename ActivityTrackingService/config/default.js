﻿'use strict';

var env = (process.env.ENV || 'default').toLowerCase();
var isProd = (env === 'prod');

var config = {
    db: {
        host: '127.0.0.1',
        port: '27017',
        username: null,
        password: null,
        database: 'activityHistory'
    },
    server: {
        name: 'Activity Tracking Service',
        ipaddress: '127.0.0.1',
        port: 9123
    },
    env: env,
    categories: [
        {
            "Name": "Coding",
            "Description": "Activities related to programming and coding are measured under this category."
        }, {
            "Name": "Meetings",
            "Description": "Time spent on meetings is measured under this category."
        },
        {
            "Name": "Email",
            "Description": "Time spent on emails is measured under this category."
        },
        {
            "Name": "Social",
            "Description": "Time spent on social networking sites are measured under this category."
        },
        {
            "Name": "Communication",
            "Description": "Time spent on offcial communication channels (Skype for Business, MS Communicator, Flowdock, etc.) are measured under this category."
        },
        {
            "Name": "Browsing",
            "Description": "Time spent on browsing is measured under this category, this includes time spent on browsing social networking sites."
        },
        {
            "Name": "Documentation",
            "Description": "Time spent on documentation using office applications like document editors, spreadsheets and presentation tools are measured under this category."
        }
    ],
    categoryMap: [
        {
            "Name": "Coding",
            "Tools": "Remote Desktop Connection|Microsoft Visual Studio 2015|SourceTree|vshost.exe|Visual Studio Code|SSMS - SQL Server Management Studio|Robomongo"
        }, {
            "Name": "Meetings",
            "Tools": "Blue Jeans Application"
        },
        {
            "Name": "Email",
            "Tools": "Microsoft Outlook"
        },
        {
            "Name": "Social",
            "Tools": "facebook|twitter|whatsapp"
        },
        {
            "Name": "Communication",
            "Tools": "Skype|Flowdock"
        },
        {
            "Name": "Browsing",
            "Tools": "Chrome|Edge|Internet Explorer"
        },
        {
            "Name": "Documentation",
            "Tools": "Microsoft PowerPoint|Microsoft Word|Microsoft Excel|Notepad|WordPad|Acrobat Reader"
        }
    ],
};

module.exports = config;