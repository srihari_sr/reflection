﻿'use strict';

var config = require('./default'),
    path = require('path');

module.exports = require(path.resolve("./config", config.env));