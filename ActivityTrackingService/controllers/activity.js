﻿'use strict';

var _ = require('underscore'),
    uuid = require('node-uuid'),
    httpStatusCodes = require('http-status-codes'),
    config = require('../config'),
    models = require('../models'),
    currentYear = new Date().getUTCFullYear();

module.exports.seed = function (req, res, next) {
    var userId = req.params.userId,
        year = Number(req.params.year);

    if (year > new Date().getUTCFullYear()) {
        res.status(httpStatusCodes.BAD_REQUEST);
        var statusMessage = 'Year cannot be greater than current year!';
        return res.send({ error: statusMessage });
    }

    var tools = [
        { "ProcessId": 10001, "Name": "Remote Desktop Connection" },
        { "ProcessId": 10002, "Name": "Microsoft Visual Studio 2015" },
        { "ProcessId": 10003, "Name": "SourceTree" },
        { "ProcessId": 10004, "Name": "vshost.exe" },
        { "ProcessId": 10005, "Name": "Visual Studio Code" },
        { "ProcessId": 10006, "Name": "SSMS - SQL Server Management Studio" },
        { "ProcessId": 10007, "Name": "Robomongo" },
        { "ProcessId": 10008, "Name": "Blue Jeans Application" },
        { "ProcessId": 10009, "Name": "Microsoft Outlook" },
        { "ProcessId": 10010, "Name": "facebook" },
        { "ProcessId": 10011, "Name": "twitter" },
        { "ProcessId": 10012, "Name": "whatsapp" },
        { "ProcessId": 10013, "Name": "Skype" },
        { "ProcessId": 10014, "Name": "Flowdock" },
        { "ProcessId": 10015, "Name": "Chrome" },
        { "ProcessId": 10016, "Name": "Edge" },
        { "ProcessId": 10017, "Name": "Internet Explorer" },
        { "ProcessId": 10018, "Name": "Microsoft PowerPoint" },
        { "ProcessId": 10019, "Name": "Microsoft Word" },
        { "ProcessId": 10020, "Name": "Microsoft Excel" },
        { "ProcessId": 10021, "Name": "Notepad" },
        { "ProcessId": 10022, "Name": "WordPad" },
        { "ProcessId": 10023, "Name": "Acrobat Reader" }
    ];

    var startRange = convertDateToUTC(new Date(year, 0, 1));
    var endRange = year === new Date().getUTCFullYear()
        ? convertDateToUTC(new Date())
        : convertDateToUTC(new Date(year, 11, 31));
    var daysOfYear = [];

    var activities = [];
    for (var date = startRange; date <= endRange; date.setDate(date.getDate() + 1)) {
        var totalTimeInADay = 28800; // 8 hours => 8 * 60 * 60 = 28800s
        for (var t = 0; t < tools.length; t++) {
            if (totalTimeInADay > 0) {
                var activityId = uuid.v4();
                var randomDuration = Math.floor(Math.random() * 10000);
                var duration = randomDuration <= totalTimeInADay ? randomDuration : 0;
                var startDate = new Date(date.valueOf());
                var activity = new models.Activity({
                    _id: activityId,
                    ActivityId: activityId,
                    ProcessId: tools[t].ProcessId,
                    FileName: tools[t].Name,
                    FileDescirption: tools[t].Name,
                    ProductName: tools[t].Name,
                    ProcessName: tools[t].Name,
                    WindowTitle: tools[t].Name,
                    ActiveWindowId: tools[t].Name,
                    StartDateTimeUtc: startDate,
                    EndDateTimeUtc: startDate.setSeconds(startDate.getSeconds() + duration),
                    Duration: duration,
                    UserId: userId
                });

                activities.push(activity);
                totalTimeInADay -= duration;
            }
        }
    }

    models.Activity.insertMany(activities, function (err, result) {
        res.send("Initialized data successfully!" + result.length.toString());
    });
};

var convertDateToUTC = function (date) {
    return new Date(
        date.getUTCFullYear(),
        date.getUTCMonth(),
        date.getUTCDate(),
        date.getUTCHours(),
        date.getUTCMinutes(),
        date.getUTCSeconds());
}

module.exports.create = function (req, res, next) {
    if (!req.body) {
        res.status(400);
        return res.send('Please provide activity details in request body.');
    }

    console.log(req.body);
    var activityId = uuid.v4();
    var activity = new models.Activity({
        _id: activityId,
        ActivityId: activityId,
        ProcessId: Number(req.body.ProcessId),
        FileName: req.body.FileName,
        FileDescirption: req.body.FileDescription,
        ProductName: req.body.ProductName,
        ProcessName: req.body.ProcessName,
        WindowTitle: req.body.WindowTitle,
        ActiveWindowId: req.body.ActiveWindowId,
        StartDateTimeUtc: Date(req.body.StartDateTimeUtc),
        EndDateTimeUtc: Date(req.body.EndDateTimeUtc),
        Duration: Number(req.body.Duration),
        UserId: req.body.UserId
    });

    activity.save(function (err) {
        if (err) {
            var statusMessage = 'Error saving activity';
            res.status(httpStatusCodes.INTERNAL_SERVER_ERROR);
            return res.send({ error: statusMessage });
        }

        statusMessage = 'Successfully saved activity.';
        res.status(httpStatusCodes.OK);
        return res.send(statusMessage);
    });
};

module.exports.getCategories = function (req, res, next) {
    res.status(httpStatusCodes.OK);
    return res.send(config.categories);
};

module.exports.getSummaryForCurrentYear = function (req, res, next) {
    var userId = req.params.user,
        startDate = getDate(currentYear, 1, 1, 0, 0, 0, 0),
        endDate = getDate(currentYear, 12, 31, 23, 59, 59, 999);
    sendSummarizedResults(userId, startDate, endDate, res);
}

module.exports.getSummaryForDate = function (req, res, next) {
    var year = Number(req.params.year),
        month = Number(req.params.month),
        date = Number(req.params.date),
        userId = req.params.user;

    if (!isValidYear(year) || !isValidMonth(month) || !isValidDate(year, month, date)) {
        res.status(httpStatusCodes.BAD_REQUEST);
        var statusMessage = 'Please provide a valid year, month and date!';
        return res.send({ error: statusMessage });
    }

    var startDate = getDate(year, month, date, 0, 0, 0, 0),
        endDate = getDate(year, month, date, 23, 59, 59, 999);
    sendSummarizedResults(userId, startDate, endDate, res);
}

module.exports.getSummaryForMonth = function (req, res, next) {
    var year = Number(req.params.year),
        month = Number(req.params.month),
        userId = req.params.user;

    if (!isValidYear(year) || !isValidMonth(month)) {
        res.status(httpStatusCodes.BAD_REQUEST);
        var statusMessage = 'Please provide a valid year and month!';
        return res.send({ error: statusMessage });
    }

    var startDate = getDate(year, month, 1, 0, 0, 0, 0),
        endDate = getDate(year, month, getDaysInMonth(year, month), 23, 59, 59, 999);
    sendSummarizedResults(userId, startDate, endDate, res);
}

module.exports.getSummaryByYear = function (req, res, next) {
    var year = Number(req.params.year),
        userId = req.params.user;

    if (!isValidYear(year)) {
        res.status(httpStatusCodes.BAD_REQUEST);
        var statusMessage = 'Please provide a valid year!';
        return res.send({ error: statusMessage });
    }

    var startDate = getDate(year, 1, 1, 0, 0, 0, 0),
        endDate = getDate(year, 12, 31, 23, 59, 59, 999);
    sendSummarizedResults(userId, startDate, endDate, res);
}

var sendSummarizedResults = function (userId, startDate, endDate, res) {
    var results = [];
    _.each(config.categoryMap, function (entry) {
        getSummaryNew(userId, startDate, endDate, entry.Name, entry.Tools, function (err, result) {
            if (result) {
                if (result.length === 0) {
                    results.push({
                        "_id": entry.Name,
                        "totalDurationInSeconds": 0
                    });
                }
                else {
                    // Push the only element in the array.
                    results.push(result[0]);
                }
            }

            if (results.length == config.categoryMap.length) {
                res.status(httpStatusCodes.OK);
                return res.send(results);
            }
        });
    });
}

var getSummaryNew = function (userId, startDate, endDate, categoryKey, toolsToQuery, next) {
    var query = [
        {
            $match: {
                $and: [{ StartDateTimeUtc: { $gte: startDate } },
                    { EndDateTimeUtc: { $lte: endDate } },
                    { "FileDescirption": { "$regex": toolsToQuery, "$options": "i" } },
                    { "FileDescirption": { $ne: "" } },
                    { "UserId": { $eq: userId } }
                ]
            }
        },
        {
            $group: {
                _id: categoryKey,
                totalDurationInSeconds: { $sum: "$Duration" }
            }
        }
    ];

    models.Activity.aggregate(query, function (err, result) {
        if (err) {
            err.statusMessage = 'Error getting summary at the moment. Please verify your query or try again later!';
            next(err, null);
        }

        return next(null, result);
    });
};

//// TODO: Move this to data / mongo utils outside of this controller
//var executeQuery = function (query, res) {
//    models.Activity.aggregate(query, function (err, result) {
//        if (err) {
//            var statusMessage = 'Error getting summary at the moment. Please verify your query or try again later!';
//            res.status(httpStatusCodes.INTERNAL_SERVER_ERROR);
//            return res.send({ error: statusMessage });
//        }

//        res.status(httpStatusCodes.OK);
//        return res.send(result);
//    });
//};

// TODO: Move this to utils module outside of the controller.
var isNumber = function (arg) {
    return typeof (arg) === 'number';
};

// TODO: Move this to utils module outside of the controller.
var getDate = function (year, month, date, hour, min, seconds, milliseconds) {
    // TODO: This is a dirty validation, needs improvement,
    // having this as temporary safety net!
    if (month > 12 || !isNumber(month)
        || date > 31 || !isNumber(date)
        || hour > 24 || !isNumber(hour)
        || min > 60 || !isNumber(min)
        || seconds > 60 || !isNumber(seconds)
        || milliseconds > 999 || !isNumber(milliseconds)) {
        return null;
    }

    // Send month-1 for month as Date.UTC represents months in 0 based index.
    return new Date(Date.UTC(year, month - 1, date, hour, min, seconds, milliseconds));
};

// TODO: Move this to utils.
var isValidYear = function (year) {
    return year && isNumber(year) && year >= 1900 && year <= currentYear;
};

// TODO: Move this to utils.
var isValidMonth = function (month) {
    return month && isNumber(month) && month >= 1 && month <= 12;
};

// TODO: Move this to utils.
var isValidDate = function (year, month, date) {
    return date > 0 && date <= getDaysInMonth(year, month);
    //var date = new Date(Date.UTC(year, month - 1, date));
    //return date.toString() != 'Invalid Date';
};

// TODO: Move this to utils.
var getDaysInMonth = function (year, month) {
    return new Date(year, month, 0).getDate();
};