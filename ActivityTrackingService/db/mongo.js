﻿var mongoose = require('mongoose'),
    config = require('../config');

var connectToDatabase = function (options) {
  var mongoUri;
  if (!options) {
    console.log('Invalid connection options');
    throw new Error('Invalid connection options');
  }
  if (options.username && options.password) {
    mongoUri = 'mongodb://' +
      options.username + ':' + options.password + '@' +
      options.host + ':' + options.port + '/' + options.database;
  } else {
    mongoUri = 'mongodb://' + options.host + ':' + options.port + '/' + options.database;
  }
  
  console.log('Connecting to mongo database: ' + mongoUri);
  var connection = mongoose.connect(mongoUri);
  
  mongoose.connection.on('connected', function () {
    console.log('Mongoose default connection open to database uri: ' + mongoUri);
  });
  
  mongoose.connection.on('error', function (err) {
    var errorMessage = 'Mongoose default connection error: ' + err;
    console.log(errorMessage);
    throw new Error(errorMessage);
  });
  
  mongoose.connection.on('disconnected', function () {
    console.log('Mongoose default connection disconnected.');
  });
  
  // Close connection once main node app server process ends
  process.on('SIGINT', function () {
    mongoose.connection.close(function () {
      console.log('Mongoose default connection disconnected through main process termination');
      process.exit(0);
    });
  });
  
  return connection;
};

exports.connect = function (env) {
  env = env || 'db';
  return connectToDatabase(config[env]);
};