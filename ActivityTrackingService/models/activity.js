﻿'use strict';

var mongoose = require('mongoose'),
    _ = require('underscore'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var activitySchema = new Schema({
    _id: false,
    UserId: { type: String, index: true },
    ActivityId: { type: String, index: true },
    ProcessId: { type: Number, index: true },
    FileName: { type: String },
    FileDescirption: { type: String },
    ProductName: { type: String },
    ProcessName: { type: String },
    WindowTitle: { type: String, index: true },
    ActiveWindowId: { type: String, index: true },
    StartDateTimeUtc: { type: Date, index: true },
    EndDateTimeUtc: { type: Date, index: true },
    Duration: { type: Number }
});

module.exports = mongoose.model('Activity', activitySchema);
