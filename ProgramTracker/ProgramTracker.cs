﻿// <copyright file="ProgramTracker.cs" company="Srihari Sridharan">
// Copyright (c) 2015 Srihari Sridharan - All Rights Reserved
// </copyright>
// <author>Srihari Sridharan</author>

namespace ProgramTracker
{
    using System;
    using System.Configuration;
    using System.Diagnostics;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Threading;
    using Newtonsoft.Json;

    /// <summary>
    /// Program tracker helps track the currently active program.
    /// </summary>
    public class ProgramTracker
    {
        /// <summary>
        /// The previous window title
        /// </summary>
        private string previousWindowTitle;

        /// <summary>
        /// The previous window information
        /// </summary>
        private WindowInfo previousWindowInfo = null;

        /// <summary>
        /// The current window information
        /// </summary>
        private WindowInfo currentWindowInfo = null;

        /// <summary>
        /// The REST helper
        /// </summary>
        private RestHelper _restHelper = new RestHelper(ConfigurationManager.AppSettings["ActivityTrackerEndpoint"], HttpVerb.POST);

        /// <summary>
        /// Writes the active window information to the console output.
        /// Only does so if the previous window title is different from the current window title.
        /// </summary>
        public void WriteActiveWindowInformation()
        {
            var activeWindowId = NativeMethodWrapper.GetForegroundWindow();

            // no (valid) foreground window => no trackable data!
            if (activeWindowId.Equals(0))
            {
                Console.WriteLine("Active Window ID is 0");
                return;
            }

            int processId;
            NativeMethodWrapper.GetWindowThreadProcessId(activeWindowId, out processId);

            // no (valid) process for window => no trackable data!
            if (processId == 0)
            {
                Console.WriteLine("Process ID is 0");
                return;
            }

            Process foregroundProcess = Process.GetProcessById(processId);

            var fileName = string.Empty;
            var fileDescription = string.Empty;
            var productName = string.Empty;
            var processName = string.Empty;
            var windowTitle = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(foregroundProcess.ProcessName))
                {
                    processName = foregroundProcess.ProcessName;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            try
            {
                if (!string.IsNullOrEmpty(foregroundProcess.MainModule.FileName))
                {
                    fileName = foregroundProcess.MainModule.FileName;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            try
            {
                if (!string.IsNullOrEmpty(foregroundProcess.MainModule.FileVersionInfo.FileDescription))
                {
                    fileDescription = foregroundProcess.MainModule.FileVersionInfo.FileDescription;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            try
            {
                if (!string.IsNullOrEmpty(foregroundProcess.MainModule.FileVersionInfo.ProductName))
                {
                    productName = foregroundProcess.MainModule.FileVersionInfo.ProductName;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            try
            {
                if (!string.IsNullOrEmpty(foregroundProcess.MainWindowTitle))
                {
                    windowTitle = foregroundProcess.MainWindowTitle;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            try
            {
                if (string.IsNullOrEmpty(windowTitle))
                {
                    const int Count = 1024;
                    var sb = new StringBuilder(Count);
                    NativeMethodWrapper.GetWindowText((int)activeWindowId, sb, Count);

                    windowTitle = sb.ToString();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            if (previousWindowTitle != windowTitle)
            {
                DateTime currentTime = DateTime.UtcNow;
                // When previous Window is not null, set the end date time and post to server.
                if (previousWindowInfo != null)
                {
                    previousWindowInfo.EndDateTimeUtc = currentTime;
                    string windowDetails = JsonConvert.SerializeObject(previousWindowInfo);
                    Console.WriteLine(windowDetails);
                    _restHelper.PostData = windowDetails;
                    string result = _restHelper.MakeRequest();
                    Console.WriteLine(result);
                }

                currentWindowInfo = new WindowInfo
                {
                    ProcessId = processId,
                    FileName = fileName,
                    FileDescription = fileDescription,
                    ProductName = productName,
                    ProcessName = processName,
                    WindowTitle = windowTitle,
                    ActiveWindowId = Convert.ToString(activeWindowId),
                    StartDateTimeUtc = currentTime
                };

                previousWindowInfo = currentWindowInfo;
                previousWindowTitle = windowTitle;
            }
        }
    }
}