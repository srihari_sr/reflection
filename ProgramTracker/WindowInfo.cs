﻿// <copyright file="WindowInfo.cs" company="Srihari Sridharan">
// Copyright (c) 2015 Srihari Sridharan - All Rights Reserved
// </copyright>
// <author>Srihari Sridharan</author>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramTracker
{
    /// <summary>
    /// Maintains the windowInformation
    /// </summary>
    public class WindowInfo
    {
        /// <summary>
        /// Gets or sets the process identifier.
        /// </summary>
        /// <value>
        /// The process identifier.
        /// </value>
        public int ProcessId { get; set; }

        /// <summary>
        /// Gets or sets the name of the file.
        /// </summary>
        /// <value>
        /// The name of the file.
        /// </value>
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the file description.
        /// </summary>
        /// <value>
        /// The file description.
        /// </value>
        public string FileDescription { get; set; }

        /// <summary>
        /// Gets or sets the name of the product.
        /// </summary>
        /// <value>
        /// The name of the product.
        /// </value>
        public string ProductName { get; set; }

        /// <summary>
        /// Gets or sets the name of the process.
        /// </summary>
        /// <value>
        /// The name of the process.
        /// </value>
        public string ProcessName { get; set; }

        /// <summary>
        /// Gets or sets the window title.
        /// </summary>
        /// <value>
        /// The window title.
        /// </value>
        public string WindowTitle { get; set; }

        /// <summary>
        /// Gets or sets the active window identifier.
        /// </summary>
        /// <value>
        /// The active window identifier.
        /// </value>
        public string ActiveWindowId { get; set; }

        /// <summary>
        /// Gets or sets the start date time in UTC.
        /// </summary>
        /// <value>
        /// The start date time in UTC.
        /// </value>
        public DateTime StartDateTimeUtc { get; set; }

        /// <summary>
        /// End date time in UTC 
        /// </summary>
        private DateTime _endDateTimeUtc;

        /// <summary>
        /// Gets or sets the end date time in UTC.
        /// </summary>
        /// <value>
        /// The end date time in UTC.
        /// </value>
        public DateTime EndDateTimeUtc
        {
            get
            {
                return _endDateTimeUtc;
            }
            set
            {
                _endDateTimeUtc = value;
                TimeSpan timeSpan = _endDateTimeUtc - StartDateTimeUtc;
                Duration = timeSpan.TotalSeconds;
            }
        }

        /// <summary>
        /// Gets the duration in seconds.
        /// </summary>
        /// <value>
        /// The duration.
        /// </value>
        public double Duration { get; private set; }

        /// <summary>
        /// Get the currently logged in user id.
        /// </summary>
        public string UserId
        {
            get
            {
                return Environment.UserName;
            }
        }
    }
}
