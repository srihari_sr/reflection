﻿// <copyright file="Program.cs" company="Srihari Sridharan">
// Copyright (c) 2015 Srihari Sridharan - All Rights Reserved
// </copyright>
// <author>Srihari Sridharan</author>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Topshelf;

namespace ProgramTracker
{
    /// <summary>
    /// Program - Startup!
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// The main entry point to the application. Starts an endless loop
        /// </summary>
        /// <param name="args">the command line arguments.</param>
        private static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                x.Service<Runner>(s =>
                {
                    s.ConstructUsing(runner => new Runner());
                    s.WhenStarted(runner => runner.Start());
                    s.WhenStopped(runner => runner.Stop());
                });

                x.RunAsLocalSystem();
                x.StartAutomatically();
                x.RunAsLocalSystem();
                x.SetDescription("Program Tracker Host");
                x.SetDisplayName("Program Tracker");
                x.SetServiceName("ProgramTracker");
            });
        }
    }

    /// <summary>
    /// Runner class for ProgramTracker
    /// </summary>
    public class Runner
    {
        private ProgramTracker _programTracker;

        /// <summary>
        /// Initializes a new instance of the <see cref="Runner"/> class.
        /// </summary>
        public Runner()
        {
            _programTracker = new ProgramTracker();
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public void Start()
        {
            while (true)
            {
                _programTracker.WriteActiveWindowInformation();
                Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        public void Stop()
        {
            _programTracker = null;
        }
    }
}
