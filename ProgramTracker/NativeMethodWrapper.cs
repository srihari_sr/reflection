﻿// <copyright file="NativeMethodWrapper.cs" company="Srihari Sridharan">
// Copyright (c) 2015 Srihari Sridharan - All Rights Reserved
// </copyright>
// <author>Srihari Sridharan</author>

using System;
using System.Runtime.InteropServices;
using System.Text;

namespace ProgramTracker
{
    /// <summary>
    /// Wraps native methods called from .NET Code.
    /// </summary>
    internal class NativeMethodWrapper
    {
        /// <summary>
        /// Gets a window handle (HWND) as an IntPtr to the currently active foreground window.
        /// </summary>
        /// <returns>Returns the window handle.</returns>
        [DllImport("user32.dll")]
        public static extern IntPtr GetForegroundWindow();

        /// <summary>
        /// Retrieves the process id of process that the window belongs to.
        /// </summary>
        /// <param name="windowHandle">the window handle to retrieve the process id for.</param>
        /// <param name="processId">the process id (return value).</param>
        /// <returns>The thread id that created the window.</returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int GetWindowThreadProcessId(IntPtr windowHandle, out int processId);

        /// <summary>
        /// Gets the text of the window specified by the hWnd window handle.
        /// </summary>
        /// <param name="hWnd">the window handle used.</param>
        /// <param name="text">a <see cref="StringBuilder"/> used to store the text. At least <see cref="count"/> characters should be reserved in the string builder instance.</param>
        /// <param name="count">the buffer length.</param>
        /// <returns>returns the number of characters copied.</returns>
        [DllImport("user32.dll")]
        public static extern int GetWindowText(int hWnd, StringBuilder text, int count);
    }
}